import React from 'react'
import { Image, ImageBackground, StyleSheet, Text, View } from 'react-native'
const backgroundImage = {
  uri: 'https://images.unsplash.com/photo-1616731948638-b0d0befef759?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'
}

export default function WelcomeScreen (props) {
  return (
    <ImageBackground style={styles.background} source={backgroundImage}>
      <View style={styles.logoContainer}>
        <Image style={styles.logo} source={require('../assets/logo.jpg')} />
        <Text>Sell What you Don't Need</Text>
      </View>

      <View style={styles.loginButton}></View>
      <View style={styles.registerButton}></View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  logoContainer: {
    top: 70,
    position: 'absolute',
    alignItems:'center'
  },
  logo: {
    width: 100,
    height: 100,
    borderRadius: 50
  },
  loginButton: {
    width: '100%',
    height: 70,
    backgroundColor: '#fc5c65'
  },

  registerButton: {
    width: '100%',
    height: 70,
    backgroundColor: '#4ecdc4'
  }
})
