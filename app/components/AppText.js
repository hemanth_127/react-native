import React from 'react'
import { Text, StyleSheet } from 'react-native'

export const AppText = ({ children }) => {
  return <Text style={styles.text}>{children}</Text>
}

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    color: 'red',
    textTransform:'lowercase'
  }
})
